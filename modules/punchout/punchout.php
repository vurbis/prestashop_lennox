<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'ModuleSetting.php');
if (!defined('_PS_VERSION_'))
    exit;
class Punchout extends Module
{
    public function __construct()
    {
        $this->name = 'punchout';
        $this->tab = 'front_office_features';
        $this->version = '2.1';
        $this->author = 'Vurbis Interactive';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Vurbis Punchout');
        $this->description = $this->l('Punchout Module!');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');


    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('submit' . $this->name)) {
            $punchout_api_user = strval(Tools::getValue('punchout_api_user'));
            $punchout_api_password = strval(Tools::getValue('punchout_api_password'));
            $punchout_supplier_code = strval(Tools::getValue('punchout_supplier_code'));
            $punchout_cxml_enable = strval(Tools::getValue('punchout_cxml_enable'));
            $punchout_oci_enable = strval(Tools::getValue('punchout_oci_enable'));
            $punchout_mode = strval(Tools::getValue('punchout_mode'));
            $error = false;
            
            if (!$punchout_api_user
                || empty($punchout_api_user)
            ) {
                $output .= $this->displayError($this->l('API User is empty.'));
                $error = true;
            }
            if (!$punchout_api_password
                || empty($punchout_api_password)
            ) {
                $output .= $this->displayError($this->l('API Password is empty.'));
                $error = true;
            }
            if (!$punchout_supplier_code
                || empty($punchout_supplier_code)
            ) {
                $output .= $this->displayError($this->l('Supplier Code is empty.'));
                $error = true;
            }
            if(!$error){
                Configuration::updateValue('punchout_api_user', $punchout_api_user);
                Configuration::updateValue('punchout_api_password', $punchout_api_password);
                Configuration::updateValue('punchout_supplier_code', $punchout_supplier_code);
                Configuration::updateValue('punchout_cxml_enable', $punchout_cxml_enable);
                Configuration::updateValue('punchout_oci_enable', $punchout_oci_enable);
                Configuration::updateValue('punchout_mode', $punchout_mode);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
        return $output . $this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('API Connection Settings'),
            ),

            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('API User'),
                    'name' => 'punchout_api_user',
                    'size' => 100,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('API Password'),
                    'name' => 'punchout_api_password',
                    'size' => 100,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Supplier Code'),
                    'name' => 'punchout_supplier_code',
                    'size' => 100,
                    'required' => false
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Punchout CXML Enable?'),
                    'name' => 'punchout_cxml_enable',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'punchout_cxml_enable_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'punchout_cxml_enable_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Punchout OCI Enable?'),
                    'name' => 'punchout_oci_enable',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'punchout_oci_enable_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'punchout_oci_enable_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Live Mode?'),
                    'name' => 'punchout_mode',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'punchout_mode_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'punchout_mode_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
			'legend' => array(
                'title' => $this->l('Email Notification'),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit' . $this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );


        $helper->fields_value['punchout_api_user'] = Configuration::get('punchout_api_user');
        $helper->fields_value['punchout_api_password'] = Configuration::get('punchout_api_password');
        $helper->fields_value['punchout_supplier_code'] = Configuration::get('punchout_supplier_code');
        $helper->fields_value['punchout_cxml_enable'] = Configuration::get('punchout_cxml_enable');
        $helper->fields_value['punchout_oci_enable'] = Configuration::get('punchout_oci_enable');
        $helper->fields_value['punchout_mode'] = Configuration::get('punchout_mode');
        
        $moduleSetting = new ModuleSetting();
        $buyerOptions = '';
        if(
            Configuration::get('punchout_api_user') && 
            Configuration::get('punchout_api_password') && 
            Configuration::get('punchout_supplier_code')
            ) {
            $buyers = $moduleSetting->getBuyers();
            $checked = '';
            foreach($buyers as $buyer) {
                if($buyer['value'] == Tools::getValue('tax_buyer')) {
                    $checked = ' selected="selected"';
                }else {
                    $checked = '';
                }
                $buyerOptions .= '<option value="'.$buyer['value'].'" '.$checked.'>'.$buyer['label'].'</option>';
            }
            
        }
        $moduleSetting = new ModuleSetting();
        if(Tools::getValue('downloadTax')=='template'){
            return $moduleSetting->generateTaxTemplate();
        }
        $upload = new UploaderCore();        
        $mimes = array('text/csv');
        $message = '';
        if(Tools::isSubmit('submitpunchout_tax')) {
            $buyer_hash = Tools::getValue('tax_buyer');
            $tax_overwrite = Tools::getValue('tax_overwrite', 0);
            if(!$buyer_hash) {
                
            }
            $file = $_FILES['tax_csv'];
            $allowed = array('csv');
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            if ( file_exists($file['tmp_name']) && in_array($extension, $allowed) )
            {
                $filename = uniqid()."-".basename($file['name']);
                $filename = str_replace(' ', '-', $filename);
                $filename = strtolower($filename);
                $filename = filter_var($filename, FILTER_SANITIZE_STRING);

                $file['name'] = $filename;

                $uploader = new UploaderCore();
                $uploader->upload($file);

                $fileUploaded = $uploader->getSavePath().$filename;
                $count = $moduleSetting->importBuyerTaxes($buyer_hash, $fileUploaded, $tax_overwrite);
                if($count) {
                    $message = "There are {$count} tax(es) updated.";                    
                }
                // remove file
                @unlink($fileUploaded);
            } 
        }
        
        $taxOverwriteChecked = Tools::getValue('tax_overwrite')== 1 ? 'checked="checked"' : '';
        $tax_form = '';
        if($message) {
            $tax_form = '<div class="module_confirmation conf confirm alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			'.$message.'
		</div>';
        }
        $tax_form  .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data" class="defaultForm form-horizontal punchout">';
        $tax_form .= '  <input name="submitpunchout_tax" value="1" type="hidden">';
        $tax_form .= '  <div class="panel" id="fieldset_tax">';
        $tax_form .= '      <div class="panel-heading">Import/Export Taxes</div>';
        $tax_form .= '      <div class="form-wrapper">'
            . '                 <div class="form-group">'
            . '                     <label class="control-label col-lg-3"></label>'                  
            . '                     <div class="col-lg-9">Download taxes template <a href="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'&downloadTax=template">here</a></div>'
            . '                 </div>'
            . '                 <div class="form-group">'            
            . '                     <label class="control-label col-lg-3 required">Buyer ID</label>'
            . '                     <div class="col-lg-9"><select name="tax_buyer">'
            .                       $buyerOptions
            . '                     </select></div>'
            . '                 </div>'
            . '                 <div class="form-group">'            
            . '                     <label class="control-label col-lg-3 required">Upload Taxes (*.csv)</label>'
            . '                     <div class="col-lg-9"><input type="file" name="tax_csv"/></div>'
            . '                 </div>'
            . '                 <div class="form-group">'
            . '                     <label class="control-label col-lg-3">Overwrite Taxes</label>'
            . '                     <div class="col-lg-9"><input type="checkbox" name="tax_overwrite" '.$taxOverwriteChecked.' value="1"/></div>'        
            . '                 </div>'                            
            . '             </div>'
            . '             <div class="panel-footer">
                                <button type="submit" value="1" id="configuration_form_submit_btn" name="submitpunchout_tax" class="btn btn-default pull-right">
                                    <i class="process-icon-save"></i>Import Tax
                                </button>
                            </div>';
        $tax_form .= '  </div>';
        $tax_form .= '</form>';
        
        return $helper->generateForm($fields_form).$tax_form;
    }
    
    public function hookDisplayAdminOrderRight($params) 
    {
        
    }

    public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'punchout.css', 'all');
	}

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install()||
        !Configuration::updateValue('punchout_api_user','')||
        !Configuration::updateValue('punchout_api_password','')||
        !Configuration::updateValue('punchout_supplier_code','')||
        !Configuration::updateValue('punchout_cxml_enable','')||
        !Configuration::updateValue('punchout_oci_enable','')||            
        !Configuration::updateValue('punchout_mode','')||
            !$this->registerHook('header')||
            !$this->registerHook('displayAdminOrderRight')
        )
            return false;

        return true;
    }
    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('punchout_api_user')||
            !Configuration::deleteByName('punchout_api_password')||
            !Configuration::deleteByName('punchout_supplier_code')||
            !Configuration::deleteByName('punchout_cxml_enable')||
            !Configuration::deleteByName('punchout_oci_enable') ||
            !Configuration::deleteByName('punchout_mode')
        )
            return false;

        return true;
    }
}