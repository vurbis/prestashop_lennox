<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutCore.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutSetting.php');
class punchoutloginModuleFrontController extends FrontController
{
    /**
     * Initialize auth controller
     * @see FrontController::init()
     */

    public function init()
    {
        parent::init();
        $id_customer = $this->context->cookie->id_customer;
        if ($id_customer) {
            $customer = new Customer($id_customer);
            $customer->logout();
            $this->context->customer = $customer;
        }
        if (!Tools::getIsset('step') && $this->context->customer->isLogged() && !$this->ajax) {
            Tools::redirect('index.php?controller='.(($this->authRedirection !== false) ? urlencode($this->authRedirection) : 'my-account'));
        }

        if (Tools::getValue('create_account')) {
            $this->create_account = true;
        }
    }


    public function initContent()
    {   
        $punchoutObj = new PunchoutCore();
        $isOci = false;
        $isCxml = false;
        if(Configuration::get('punchout_cxml_enable') && Tools::getValue('hash')) {
            $isCxml = true;
        }
        if(Configuration::get('punchout_oci_enable') && Tools::getValue('HOOK_URL')) {
            $isOci = true;
        }
        if(!$isCxml && !$isOci)
        {
            Tools::redirect('index.php?controller='.(($this->authRedirection !== false) ? urlencode($this->authRedirection) : 'my-account'));
        }else {
            
            $passwd = trim(Tools::getValue('password'));
            $email = trim(Tools::getValue('username'));
            $user_id = trim(Tools::getValue('user_id'));
            $firstname = trim(Tools::getValue('firstname', ''));
            $lastname = trim(Tools::getValue('lastname', ''));
            if($isCxml) {
                $hash   = trim(Tools::getValue('hash'));
                $items = Tools::getValue('items');
                $items  = json_decode(base64_decode($items));
            }
            if($isOci) {
                $return_target = Tools::getValue('returntarget') ? Tools::getValue('returntarget') : '_top';
                if(!$user_id) {
                    $user_id = $punchoutObj->getRandomString(rand(8,20)).$email;
                }
            }
            if($user_id && !Validate::isEmail($user_id)) {
                $this->errors[] = Tools::displayError('Invalid UserID email address.');
            }else {
                                
                if ($passwd && $email) {
                    if (empty($email)) {
                        $this->errors[] = Tools::displayError('An email address required.');
                    } elseif (!Validate::isEmail($email)) {
                        $this->errors[] = Tools::displayError('Invalid email address.');
                    } elseif (empty($passwd)) {
                        $this->errors[] = Tools::displayError('Password is required.');
                    } elseif (!Validate::isPasswd($passwd)) {
                        $this->errors[] = Tools::displayError('Invalid password.');
                    } else {
                        Hook::exec('actionBeforeAuthentication');
                        $customer = new Customer();
                        $authentication = $customer->getByEmail(trim($email), trim($passwd));
                        $groups = $authentication->getGroups();
                        if (isset($authentication->active) && !$authentication->active) {
                            $this->errors[] = Tools::displayError('Your account isn\'t available at this time, please contact us');
                        } elseif (!$authentication || !$customer->id) {
                            $this->errors[] = Tools::displayError('Authentication failed.');
                        } else {
                            if(!Customer::customerExists($user_id))
                            {
                                // create new customer
                                $dataPost = [
                                    'email'=>$user_id
                                ];
                                $newCustomer = new Customer();
                                $newCustomer = $authentication;
                                $newCustomer->email = $user_id;
                                $newCustomer->firstname = $firstname;
                                $newCustomer->lastname = $lastname;    
                                $newCustomer->punchout_is_delete = 1;
                                $newCustomer->add();
                                
                                $newCustomer->updateGroup($groups);
                                Hook::exec('actionCustomerAccountAdd', array(
                                '_POST' => $dataPost,
                                'newCustomer' => $newCustomer
                            ));
                            }

                            // login
                            $punchoutCustomer = new Customer();
                            // enable customer

                            $_customer = $punchoutCustomer->getByEmail(trim($user_id));
                            $_customer->active = 1;
                            $_customer->save();

                            $authentication = $punchoutCustomer->getByEmail(trim($user_id), trim($passwd));

                            $this->context->cookie->id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($punchoutCustomer->id);
                            $this->context->cookie->id_customer = (int)($punchoutCustomer->id);
                            $this->context->cookie->customer_lastname = $punchoutCustomer->lastname;
                            $this->context->cookie->customer_firstname = $punchoutCustomer->firstname;
                            $this->context->cookie->logged = 1;
                            $punchoutCustomer->logged = 1;
                            $this->context->cookie->is_guest = $punchoutCustomer->isGuest();
                            $this->context->cookie->passwd = $punchoutCustomer->passwd;
                            $this->context->cookie->email = $punchoutCustomer->email;
                            $buyer_hashcode = $punchoutCustomer->buyer_id;
                            $this->context->cookie->is_punchout_logged_in = 1;
                            $this->context->cookie->punchout_delete_user = 1;
                            
                            $logData = [
                                'supplier_hashcode'=>Configuration::get('punchout_supplier_code'),
                                'buyer_hashcode'=>$buyer_hashcode,
                                'customer_id'=>$punchoutCustomer->id,                                
                            ];
                            $logEntity = $punchoutObj->createLoginLog($logData);
                            if($logEntity) {
                                $this->context->cookie->log_hash = $logEntity->hashcode;
                            }
                            
                            if($isCxml) {
                                $this->context->cookie->punchout_hash = $hash;
                            }
                            if($isOci) {
                                $this->context->cookie->hook_url = Tools::getValue('HOOK_URL');
                                $this->context->cookie->returntarget = $return_target;
                            }
                            $punchoutCustomer->is_punchout_logged_in = 1;
                            // Add customer to the context
                            $this->context->customer = $punchoutCustomer;
                            
                            //clear cart
                            $this->context->cart->delete();
                            
                            // reload cart
                            
                            

                            if(is_array($items))
                            {
                                // create cart
                                $this->createCart();
                                foreach($items as $item)
                                {
                                    $quantity = $item->qty;
                                    $id_product = $item->id;
                                    if(strpos($id_product,'#')!==false)
                                    {
                                        $productIds = explode('#',$id_product);
                                        $id_product = $productIds[0];
                                        $id_product_attribute = $productIds[1];
                                        $this->context->cart->updateQty($quantity, $id_product, $id_product_attribute, false);
                                    }else {
                                        $this->context->cart->updateQty($quantity, $id_product, null, false);
                                    }
                                }
                                
                            }

                            if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart) || Cart::getNbProducts($this->context->cookie->id_cart) == 0) && $id_cart = (int)Cart::lastNoneOrderedCart($this->context->customer->id)) {
                                $this->context->cart = new Cart($id_cart);
                            } else {

                                $id_carrier = (int)$this->context->cart->id_carrier;
                                $this->context->cart->id_carrier = 0;
                                $this->context->cart->setDeliveryOption(null);
                                $this->context->cart->id_address_delivery = (int)Address::getFirstCustomerAddressId((int)($punchoutCustomer->id));
                                $this->context->cart->id_address_invoice = (int)Address::getFirstCustomerAddressId((int)($punchoutCustomer->id));

                            }
                            $this->context->cart->id_customer = (int)$punchoutCustomer->id;
                            $this->context->cart->secure_key = $punchoutCustomer->secure_key;

                            if ($this->ajax && isset($id_carrier) && $id_carrier && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                                $delivery_option = array($this->context->cart->id_address_delivery => $id_carrier.',');
                                $this->context->cart->setDeliveryOption($delivery_option);
                            }

                            $this->context->cart->save();
                            $this->context->cookie->id_cart = (int)$this->context->cart->id;
                            $this->context->cookie->write();
                            $this->context->cart->autosetProductAddress();

                            Hook::exec('actionAuthentication', array('customer' => $this->context->customer));

                            // Login information have changed, so we check if the cart rules still apply
                            CartRule::autoRemoveFromCart($this->context);
                            CartRule::autoAddToCart($this->context);

                            if (!$this->ajax) {
                                $back = Tools::getValue('back','index');

                                if ($back == Tools::secureReferrer($back)) {
                                    Tools::redirect(html_entity_decode($back));
                                }

                                Tools::redirect('index.php?controller='.(($this->authRedirection !== false) ? urlencode($this->authRedirection) : $back));
                            }
                        }
                    }
                }
            }
            return AuthControllerCore::initContent();
        }
    }

    public function createCart()
    {
        if (is_null($this->context->cart)) {
            $this->context->cookie->id_cart = null;
            $this->context->cart =
                new Cart($this->context->cookie->id_cart);
        }

        if (is_null($this->context->cart->id_lang)) {
            $this->context->cart->id_lang = $this->context->cookie->id_lang;
        }

        if (is_null($this->context->cart->id_currency)) {
            $this->context->cart->id_currency = $this->context->cookie->id_currency;
        }

        if (is_null($this->context->cart->id_customer)) {
            $this->context->cart->id_customer = $this->context->cookie->id_customer;
        }

        if (is_null($this->context->cart->id_guest)) {

            if (empty($this->context->cookie->id_guest)){
                $this->context->cookie->__set(
                    'id_guest',
                    Guest::getFromCustomer($this->context->cookie->id_customer)
                );
            }
            $this->context->cart->id_guest = $this->context->cookie->id_guest;
        }

        if (is_null($this->context->cart->id)) {

            $this->context->cart->add();

            $this->context->cookie->__set('id_cart', $this->context->cart->id);
        }
    }

    protected function processSubmitLogin()
    {
        $this->context->cookie->is_punchout_logged_in = null;
        $this->context->cookie->punchout_hash = null;
        return AuthControllerCore::processSubmitLogin();
    }
}
