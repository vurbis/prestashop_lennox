<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'api'.DIRECTORY_SEPARATOR.'bootstrap.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
class punchoutrequestModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $xml = file_get_contents('php://input');
        $rest = $this->getRestApi('cxml-punchout-setup-request');
        $data['request'] = $xml;
        $supplier_hashcode = Configuration::get('punchout_supplier_code');
        $is_live = Configuration::get('punchout_mode');
        $data['is_live'] = $is_live;
        $data['supplier_hashcode'] = $supplier_hashcode;
        $entity = $rest->addEntity($data);
        $xml=simplexml_load_string($xml) or die("Error: Cannot create object");
        header('content-type:text/xml');
        echo $entity->response;
        exit;
    }

    public function getRestApi($resource)
    {
        $entity = new Entity();
        return $entity->getRestApi($resource);
    }

}