<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'api'.DIRECTORY_SEPARATOR.'bootstrap.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutCore.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutSetting.php');
class punchoutajaxociModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $punchoutCore = new PunchoutCore();
        
        // logout customer
        $id_customer = $this->context->cookie->id_customer;
       /*
        if ($id_customer) {
            $customer = new Customer($id_customer);
            $customer->logout();
            if($this->context->cookie->punchout_delete_user) {
                $customer->delete();
            }
            $this->context->customer = $customer;
        }
        */
        $cartData = $this->context->cart->getFields();
        $log = Tools::getValue('log');
        $log_data = array(
            'cart_content'=>$log,
            'quote_id'=>$cartData['id_cart'],
            'date_submit_order'=>date('Y-m-d H:i:s'),
            'finish_order'=>1
        );
        $logResource = $punchoutCore->getRestApi('punchout-logs');
        $entity = $logResource->updateEntity($this->context->cookie->log_hash, $log_data);
        
        // clear cart session
        $clear_cart = Tools::getValue('clear_cart');
        if($clear_cart) {
            $this->context->cookie->id_cart = null;
        }
        
        $close_window = 1;
        $hook_target = $this->context->cookie->returntarget;
        $result = ['refresh'=>'0','close_window'=>$close_window,'account'=>$id_customer];
        if ($hook_target !='_top') {
            $result['refresh'] = '1';
        } else {
            $result['refresh'] = '0';
        }
        $result['completed'] = 1;
        
        
         die( Tools::jsonEncode($result));        
    }
    
}