<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'api'.DIRECTORY_SEPARATOR.'bootstrap.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutCore.php');
class punchoutajaxcxmlModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $rest = $this->getRestApi('cxml-punchout-setup-request');
        $data['result'] = 'OK';
        $order_message = Tools::getValue('order_message');
        $update = array(
            'order_message'=>$order_message
        );
        $id_customer = $this->context->cookie->id_customer;
       
        if ($id_customer) {
            $customer = new Customer($id_customer);
            $customer->mylogout();
            
        }
        $cart = new Cart($this->context->cookie->id_cart);
        $cart->id_customer = null;
        $cart->save();
        $this->context->cookie->id_cart = null;
        
        $request_hash = $this->context->cookie->punchout_hash;

        $rest->updateEntity($request_hash,$update);
        
        $punchoutCore = new PunchoutCore();
        $cartLog = Tools::getValue('cart_log');
        if($cartLog) {
        	$punchoutCore->removeCxmlCartLog($cartLog);
        }

        die( Tools::jsonEncode($data));
    }
    public function getRestApi($resource)
    {
        $entity = new Entity();
        return $entity->getRestApi($resource);
    }
}