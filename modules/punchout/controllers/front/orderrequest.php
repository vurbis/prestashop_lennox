<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'api'.DIRECTORY_SEPARATOR.'bootstrap.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Purchaseorder.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
class punchoutorderrequestModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {        
	$po = new Purchaseorder();
        try {
            $xml = file_get_contents('php://input');
            $rest = $this->getRestApi('cxml-punchout-order-setup-request');
            $data['request'] = $xml;
            $supplier_hashcode = Configuration::get('punchout_supplier_code');
            $is_live = Configuration::get('punchout_mode');

            $data['is_live'] = $is_live;
            $data['supplier_hashcode'] = $supplier_hashcode;
            $entity = $rest->addEntity($data);
            // creating order

            $orderEntity = $rest->getEntity($entity->hashcode);
            if($orderEntity)
            {
                $order = $orderEntity->order;            
                $po->setContext($this->context);
                $isCreatedOrder = $po->createOrder($order, $xml);
                if($isCreatedOrder)
                {
                    $response = $po->getSuccessMessage();
                    $updateData = array(
                        'order_completed'=>'1',
                        'order_created_at'=>date('Y-m-d H:i:s'),
                        'response_xml'=>$response,
                    );
                    $rest->updateEntity($entity->hashcode,$updateData);
                }else{
                    $response = $po->getErrorMessage(406, 'Can not create order');
                    $updateData = array(
                        'order_completed'=>'0',
                        'order_created_at'=>date('Y-m-d H:i:s'),
                        'response_xml'=>$response
                    );
                    $rest->updateEntity($entity->hashcode,$updateData);
                    throw new \Exception("Can not create order");
                }
                $rest->updateEntity($entity->hashcode,$updateData);
            }
        } catch(\Exception $e) {
            $response = $po->getErrorMessage(406, $e->getMessage());
        }
        header('content-type:text/xml');
        echo $response;
        exit;
    }



    public function getRestApi($resource)
    {
        $entity = new Entity();
        return $entity->getRestApi($resource);
    }

}
