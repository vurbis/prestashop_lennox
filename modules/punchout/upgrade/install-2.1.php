<?php

if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_2_1($module)
{
    $sql = '
    ALTER TABLE `'._DB_PREFIX_.'order_detail` 
    ADD `line_number` int(11) NULL DEFAULT \'0\' After product_reference';
    Db::getInstance()->execute($sql);
    return true;
}
