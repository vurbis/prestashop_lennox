<?php
ini_set('display_errors', true);
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutCore.php');
class Purchaseorder
{
    protected $context;
    protected $cartLog;
    const STATUS = 1;
    const ORDER_CANCEL_STATUS = 6;
    const DEBUG_MODE = false;

    public function setContext($context)
    {
        $this->context = $context;
    }
    public function createOrder($data, $xml)
    {
        try {
            $this->cartLog = null;
            // check customer
            $email = $data->contact->Email;
            $customer_name = $data->contact->Name;
            $po_number = $data->orderId;
            $type = $data->type;
            if($type == 'update') {
                $orders = $this->getOrderIdsByPoNumber($po_number);
                foreach($orders as $_order) {
                    $orderId = $_order['id_order'];
                    $order = new Order($orderId);
                    if($order) {
                        $order->setCurrentState(self::ORDER_CANCEL_STATUS);
                    }
                }
            }
            
            // validate order items
            // to check if the sku is missing in order item line
            // then send message
            if($this->isFreeTextOrder($data, $xml)){
                return true;
            }
            
            $customer_id = Customer::customerExists($email,true);
            $paymentDecode = unserialize($data->payment_method);
            $paymentModule = $paymentDecode['code'];
            $paymentName = $paymentDecode['name'];
            $shippingMethod = $data->shipping_method;
            if(!$customer_id)
            {
                // create new customer
                $names = explode(' ',$customer_name);

                $firstname = $names[0];
                $lastname = substr($customer_name,strlen($firstname)+1);

                $newCustomer = new Customer();

                $newCustomer->email = $email;
                $newCustomer->firstname = $firstname;
                $newCustomer->lastname = $lastname;
                $newCustomer->active=1;
                $newCustomer->passwd = md5(time());
                $newCustomer->is_guest = 0;
                $newCustomer->add();
                $customer_id = $newCustomer->id;

                $this->context->cookie->id_customer = $newCustomer->id;
                $this->context->cookie->customer_lastname = $newCustomer->lastname;
                $this->context->cookie->customer_firstname = $newCustomer->firstname;

                // create billing address
                $billing = $data->billing;
                $billingAddress = new AddressCore();
                $billingAddress->firstname = $firstname;
                $billingAddress->lastname = $lastname;
                $billingAddress->company = $billing->Name;
                for($i=0;$i<count($billing->Street);$i++)
                {
                    $billingAddress->{'address'.($i+1)} = $billing->Street[$i];
                }
                $billing_country = CountryCore::getByIso($billing->CountryCode,1);
                $billingAddress->city = $billing->City;
                $billingAddress->postcode = $billing->PostalCode;
                $billingAddress->alias = $billing->DeliverTo;
                $billingAddress->id_country = $billing_country;
                $billing_state = StateCore::getIdByIso($billing->State,$billing_country);
                if($billing_state) {
                    $billingAddress->id_state = $billing_state;
                }else{
                    $billingAddress->other = $billing->State;
                }
                $billingAddress->id_customer = $newCustomer->id;
                $billingAddress->phone = '+'.$billing->Phone->CountryCode.' '.$billing->Phone->AreaOrCityCode.' '.$billing->Phone->Number;
                $billingAddress->add();
                $billing_id = $billingAddress->id;

                // create shipping address
                $shipping = $data->shipping;
                $shippingAddress = new AddressCore();
                $shippingAddress->firstname = $firstname;
                $shippingAddress->lastname = $lastname;
                $shippingAddress->company = $shipping->Name;
                for($i=0;$i<count($shipping->Street);$i++)
                {
                    $shippingAddress->{'address'.($i+1)} = $shipping->Street[$i];
                }
                $shipping_country = CountryCore::getByIso($shipping->CountryCode,1);

                $shippingAddress->city = $shipping->City;
                $shippingAddress->postcode = $shipping->PostalCode;
                $shippingAddress->alias = $shipping->DeliverTo;
                $shippingAddress->id_country = $shipping_country;
                $shipping_state = StateCore::getIdByIso($shipping->State,$shipping_country);
                if($shipping_state) {
                    $shippingAddress->id_state = $shipping_state;
                }else{
                    $shippingAddress->other = $shipping->State;
                }
                $shippingAddress->phone = '+'.$shipping->Phone->CountryCode.' '.$shipping->Phone->AreaOrCityCode.' '.$shipping->Phone->Number;
                $shippingAddress->id_customer = $newCustomer->id;
                $shippingAddress->add();
                $shipping_id = $shippingAddress->id;

                $newCustomer->id_delivery_address = $shipping_id;
                $newCustomer->id_invoice_address = $billing_id;
                $newCustomer->save();


            }else{
                $customer = Customer::getCustomersByEmail($email);
                
                $customer_id = $customer[0]['id_customer'];
                $billing_id = Address::getFirstCustomerAddressId($customer_id, true);
                $shipping_id = Address::getFirstCustomerAddressId($customer_id, true);
                
                $names = explode(' ',$customer_name);

                $firstname = $names[0];
                $lastname = substr($customer_name,strlen($firstname)+1);

                $newCustomer = new Customer($customer_id);
                $newCustomer->firstname = $firstname;
                $newCustomer->lastname = $lastname;
                $billingAddress = Address::addressExists($billing_id);
                if(!$billing_id)
                {
                    // create billing address
                    $billing = $data->billing;
                    $billingAddress = new AddressCore();
                    $billingAddress->firstname = $firstname;
                    $billingAddress->lastname = $lastname;
                    $billingAddress->company = $billing->Name;
                    for($i=0;$i<count($billing->Street);$i++)
                    {
                        $billingAddress->{'address'.($i+1)} = $billing->Street[$i];
                    }
                    $billing_country = CountryCore::getByIso($billing->CountryCode,1);
                    
                    $billingAddress->city = $billing->City;
                    $billingAddress->postcode = $billing->PostalCode;
                    $billingAddress->alias = $billing->DeliverTo;
                    $billingAddress->id_country = $billing_country;
                    $billing_state = StateCore::getIdByIso($billing->State,$billing_country);
                    if($billing_state) {
                        $billingAddress->id_state = $billing_state;
                    }else{
                        $billingAddress->other = $billing->State;
                    }
                    $billingAddress->id_customer = $customer_id;
                    $billingAddress->phone = '+'.$billing->Phone->CountryCode.' '.$billing->Phone->AreaOrCityCode.' '.$billing->Phone->Number;
                    $billingAddress->add();
                    $billing_id = $billingAddress->id;
                    $newCustomer->id_delivery_address = $shipping_id;
                    $newCustomer->id_invoice_address = $billing_id;
                }

                if(!$shipping_id)
                {
                    // create shipping address
                    $shipping = $data->shipping;
                    $shippingAddress = new AddressCore();
                    $shippingAddress->firstname = $firstname;
                    $shippingAddress->lastname = $lastname;
                    $shippingAddress->company = $shipping->Name;
                    for($i=0;$i<count($shipping->Street);$i++)
                    {
                        $shippingAddress->{'address'.($i+1)} = $shipping->Street[$i];
                    }
                    $shipping_country = CountryCore::getByIso($shipping->CountryCode,1);

                    $shippingAddress->city = $shipping->City;
                    $shippingAddress->postcode = $shipping->PostalCode;
                    $shippingAddress->alias = $shipping->DeliverTo;
                    $shippingAddress->id_country = $shipping_country;
                    $shipping_state = StateCore::getIdByIso($shipping->State,$shipping_country);
                    if($shipping_state) {
                        $shippingAddress->id_state = $shipping_state;
                    }else{
                        $shippingAddress->other = $shipping->State;
                    }
                    $shippingAddress->phone = '+'.$shipping->Phone->CountryCode.' '.$shipping->Phone->AreaOrCityCode.' '.$shipping->Phone->Number;
                    $shippingAddress->id_customer = $customer_id;
                    $shippingAddress->add();
                    $shipping_id = $shippingAddress->id;
                    $newCustomer->id_delivery_address = $shipping_id;
                }
                $newCustomer->save();

            }
            
            
            
            // set Customer ID
            $this->context->cookie->id_customer = $customer_id;
            
           	// validate order items
           
            
            $this->createCart();
            $this->context->cart->id_customer = $customer_id;
            $this->context->cart->update();
            $this->context->cart->setNoMultishipping();
            $this->context->cart->id_address_delivery = $shipping_id;
            $this->context->cart->id_address_invoice = $billing_id;
            $this->context->cart->update();
            foreach($data->items as $item) {
            	$this->addCartItem($item);
            }
            
            $cart = new Cart($this->context->cookie->id_cart);
            $payment_module = Module::getInstanceByName($paymentModule);
            $payment_module->validateOrder(
                (int)$cart->id,
                (int)self::STATUS,
                $cart->getOrderTotal(true, Cart::BOTH), 
                $payment_module->displayName,
                'PO Order: '.$po_number, 
                array('transaction_id'=>$po_number, 'po_number'=>$po_number), 
                null, 
                false, 
                $cart->secure_key
            );
            if ($payment_module->currentOrder) {
                $order = new Order($payment_module->currentOrder);
                $order->po_number = $po_number;
                $order->save();
                
                return true;
            } else {
                return false;
            }
        	
            
            
            
        }catch(PrestaShopException $e) {
        	throw new \Exception($e->getMessage());
        }
        catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }
    
    protected function isFreeTextOrder($data, $xml)
    {
    	//$email = 'airbus@afso.fr';
        $email = 'dat@vurbis.com';
        $errors = [];
        $items = '<table border="1" cellspacing="0" cellpadding="5" width="100%">';
        $items .= ''
            . ' <tr>'
            . '     <td>Supplier Part ID</td>'
            . '     <td>Description</td>'
            . '     <td>Price</td>'
            . '     <td>Qty</td>'
            . '     <td>Line number</td>'
            . '     <td>cf_building_room</td>'
            . '     <td>Comments</td>'
            . ' </tr>';
    	foreach($data->items as $item) {
            $SupplierPartAuxiliaryID = trim($item->SupplierPartAuxiliaryID);
            $lineNumber = $item->lineNumber;
            if(!$SupplierPartAuxiliaryID) {
                    $errors[] = sprintf('Product code missed at line number %s.', $lineNumber);
            }
            $cfBuildingRoom = isset($item->Extrinsic->cf_building_room) ? $item->Extrinsic->cf_building_room : '';
            $items .= ''
                . ' <tr>'
                . '     <td>'.$item->SupplierPartID.'</td>'
                . '     <td>'.$item->Description.'</td>'
                . '     <td>'.$item->Price.'</td>'
                . '     <td>'.$item->qty.'</td>'
                . '     <td>'.$lineNumber.'</td>'
                . '     <td>'.$cfBuildingRoom.'</td>'
                . '     <td>'.$item->Comments.'</td>'
                . '</tr>';
    	}
        $items .= '</table>';
        $message = '<table border="1" cellspacing="0" cellpadding="5" width="100%">';
        $message .= $this->setText( 'Type',$data->type);
        $message .= $this->setText( 'PO Number',$data->orderId);
        $message .= $this->setText( 'Order Total',$data->total);
        $message .= $this->setText( 'Order Currency',$data->currency);
        $message .= $this->setText( 'Comments',$data->Comments);
        
        $carrier = new Carrier($data->shipping_method);
        $message .= $this->setText( 'Shipping Method',$carrier->name);
        $paymentDecode = unserialize($data->payment_method);
        
        $paymentName = $paymentDecode['name'];
        $message .= $this->setText( 'Payment Method',$paymentName);
        
        $customer_email = $data->contact->Email;
        $customer_name = $data->contact->Name;
        $message .= $this->setText( 'Customer Email',$customer_email);
        $message .= $this->setText( 'Customer Name',$customer_name);
        
        $shipping = $data->shipping;
        $message .= $this->setText( 'Shipping Address');
        $message .= $this->setText( 'Company', $shipping->Name);
        for($i=0;$i<count($shipping->Street);$i++) {
            $message .= $this->setText( 'Street '.($i+1), $shipping->Street[$i]);
        }
        $message .= $this->setText( 'Alias', $shipping->DeliverTo);
        $message .= $this->setText( 'City', $shipping->City);
        if($shipping->State) {
            $message .= $this->setText( 'State', $shipping->State);
        }
        $message .= $this->setText( 'Post Code', $shipping->PostalCode);
        $message .= $this->setText( 'Country', $shipping->Country);
        
        $billing = $data->billing;
        $message .= $this->setText( 'Billing Address');
        $message .= $this->setText( 'Company', $billing->Name);
        for($i=0;$i<count($billing->Street);$i++) {
            $message .= $this->setText( 'Street '.($i+1), $billing->Street[$i]);
        }
        $message .= $this->setText( 'Alias', $billing->DeliverTo);
        $message .= $this->setText( 'City', $billing->City);
        if($billing->State) {
            $message .= $this->setText( 'State', $billing->State);
        }
        $message .= $this->setText( 'Post Code', $billing->PostalCode);
        $message .= $this->setText( 'Country', $billing->Country);
        $message .= '</table>';
        
        if(count($errors) > 0) {
            $fileAttachment['content'] = $xml;
            $fileAttachment['name'] = $data->orderId.'.xml'; 
            $fileAttachment['mime'] = 'application/xml';
            $po = $data->orderId;
            Mail::Send(
                (int)Context::getContext()->cookie->id_lang,
                'punchout_freetext',
                Mail::l('[Free text order] - PO: '.$po, (int)Context::getContext()->cookie->id_lang),
                [ '{error_message}'=> implode('<br/>', $errors), '{order}'=>$message, '{items}'=>$items],
                $email,
                null,
                $email,
                null,
                $fileAttachment);
            return true;
    	}
        return false;
    }
    
    private function setText($text, $value = '')
    {
        if($value) {
            return '<tr><td width="30%">'.$text.'</td><td>'.$value.'</td></tr>';
        } else {
            return '<tr><td colspan="2">'.$text.'</td></tr>';
        }
    }

    public function createCart()
    {       
        
    	if (!$this->context->cart->id) {
    		if (Context::getContext()->cookie->id_guest) {
    			$guest = new Guest(Context::getContext()->cookie->id_guest);
    			$this->context->cart->mobile_theme = $guest->mobile_theme;
    		}
    		$this->context->cart->add();
    		if ($this->context->cart->id) {
    			$this->context->cookie->id_cart = (int)$this->context->cart->id;
    		}
    	}
    	
    }
    
    protected function addCartItem($item) 
    {
    	$qty = $item->qty;
    	$lineNumber = $item->lineNumber;
    	$uuid = trim($this->getItemUuid($item));
    	$item_id = trim($this->getItemId($item));
    	try{
            if($uuid) {
                $cartLog = $this->getCartLog($uuid);
                $logItems = json_decode($cartLog->quote_item_json, true);
                foreach($logItems as $logItem) {
                    if($logItem['item_id'] == $item_id){
                        $id_product = $logItem['id_product'];
                        $id_product_attribute = $logItem['id_product_attribute'];
                        $id_customization = $logItem['id_customization'];
                        $product = new Product($id_product, true, $this->context->language->id);
                        $update_quantity = $this->context->cart->updateQty($qty, $id_product, $id_product_attribute, $id_customization);
                        $error = '';
                        if ($update_quantity < 0) {
                                // If product has attribute, minimal quantity is set with minimal quantity of attribute
                                $minimal_quantity = ($id_product_attribute) ? Attribute::getAttributeMinimalQty($id_product_attribute) : $product->minimal_quantity;
                                $error = sprintf('You must add %d minimum quantity for product at line number %d.', $minimal_quantity, $lineNumber);
                        } elseif (!$update_quantity) {
                                $error = sprintf('You already have the maximum quantity available for this product at line number %d.', $lineNumber);
                        }
                        if($error) {
                                throw new PrestaShopException($error);
                        }
                        break;
                    }
                }
            } else {
                $SupplierPartID = $item->SupplierPartID;
                $id_product = $this->findProductByReference($SupplierPartID);
                if($id_product) {
                    $product = new Product($id_product, true, $this->context->language->id);
                    $update_quantity = $this->context->cart->updateQty($qty, $id_product);
                    $error = '';
                    if ($update_quantity < 0) {
                        // If product has attribute, minimal quantity is set with minimal quantity of attribute
                        $minimal_quantity = ($id_product_attribute) ? Attribute::getAttributeMinimalQty($id_product_attribute) : $product->minimal_quantity;
                        $error = sprintf('You must add %d minimum quantity for product at line number %d.', $minimal_quantity, $lineNumber);
                    } elseif (!$update_quantity) {
                        $error = sprintf('You already have the maximum quantity available for this product at line number %d.', $lineNumber);
                    }
                    if($error) {
                        throw new PrestaShopException($error);
                    }
                }
            }
    	} catch(\Exception $e) {
    		
    	}
    	$this->context->cart->save();
    	
    }
    
    
    protected function getCartLog($uuid) {
    	if(!$this->cartLog) {
    		$punchoutCore = new PunchoutCore();
    		$this->cartLog = $punchoutCore->getCxmlCartLog($uuid);
    	}
    	return $this->cartLog;
    }
    
    protected function getItemUuid($item) 
    {
        if(isset($item->SupplierPartAuxiliaryID)) {
            $SupplierPartAuxiliaryID = $item->SupplierPartAuxiliaryID;    	
            if(strpos($SupplierPartAuxiliaryID, '|') !== false) {
                return explode('|', $SupplierPartAuxiliaryID)[0];
            }
        }
    	return false; 
    }
    
    protected function getItemId($item)
    {
    	$SupplierPartAuxiliaryID = $item->SupplierPartAuxiliaryID;
    	return explode('|', $SupplierPartAuxiliaryID)[1];
    }
    
    protected function getOrderIdsByPoNumber($po) 
    {
        $sql = 'SELECT `id_order`
				FROM `'._DB_PREFIX_.'orders`
				WHERE `po_number` = "'.$po.'"';
        $result = Db::getInstance()->executeS($sql);

        return $result;
    }
	
    public function findProductByReference($reference) 
    {
    	$sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE reference = \''.$reference.'\'';
       	$row = Db::getInstance()->getValue($sql);
       	return $row;
    }

    /**
     * Get Success Cxml Message
     * @return string
     */
    public function getSuccessMessage()
    {
        $responseXml = '<?xml version="1.0"?>';
        $payloadId = time();
        $responseXml .= '<cXML payloadID="'.$payloadId.'"  xml:lang="en" timestamp="'.date('c').'">
                            <Response>
                                <Status code="200" text="OK"/>                                    
                            </Response>
                        </cXML>';
        return $responseXml;
    }
    
    /**
     * Generate Error message
     * @param string $code
     * @param string $message
     * @return string
     */
    public function getErrorMessage($code, $message)
    {
        $responseXml = '<?xml version="1.0"?>';
        $payloadId = time();
        $responseXml .= '<cXML payloadID="'.$payloadId.'" xml:lang="en" timestamp="'.date('c').'">
                            <Response>
                                    <Status code="'.$code.'" text="Not Acceptable">'.$message.'</Status>                                    
                                </Response>
                            </cXML>';
        return $responseXml;
    }
}