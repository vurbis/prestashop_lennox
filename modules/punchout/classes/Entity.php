<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'api'.DIRECTORY_SEPARATOR.'bootstrap.php');
class Entity 
{
    const API_OAUTH2 = 'https://api.vurbismarketplace.com/oauth';
    const API_URL = 'https://api.vurbismarketplace.com/v2/';
    public function getRestApi($resource)
    {

        $username = Configuration::get('punchout_api_user');
        $password = Configuration::get('punchout_api_password');
        $oauth2_url = self::API_OAUTH2;
        $resource_url = self::API_URL.$resource;

        $oauth_client = new Vurbis\API\Oauth2\Client($oauth2_url,$username,$password,$username,$password);
        $oauth_client->authorize();
        $rest = new Vurbis\API\Rest\Client($resource_url);
        $rest->setOauthClient($oauth_client);
        return $rest;
    }

    public function isLiveMode()
    {
        if(Configuration::get('punchout_mode'))
        {
            return true;
        }
        return false;
    }
}