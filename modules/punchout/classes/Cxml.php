<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutCore.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutSetting.php');
class Cxml extends PunchoutCore
{
    private $context;
    
    public function __construct($context) {
        $this->context = $context;
    }
    /**
     * @param $context
     */
    public function generateCxmlPunchoutForm()
    {
        $context = $this->context;
        $customer = $context->customer;
        $punchout_hash = $context->cookie->punchout_hash;
        $resource = $this->getRestApi('cxml-punchout-order-message');
        $supplier_hashcode = Configuration::get('punchout_supplier_code');
        $is_live = Configuration::get('punchout_mode');
        $buyer_hashcode = $customer->buyer_id;

        $punchoutSetting = new PunchoutSetting();
        $punchout_setting = $punchoutSetting->getPunchoutSetting($buyer_hashcode);
        $punchoutCore = new PunchoutCore();
        
        $uuid = $punchoutCore->getUuid();
        
        if(!$punchout_setting){
            return;
        }
        $items = array();
        $cartData = $context->cart->getFields();
        $products = [];
        $orderedOptions = []; 
        foreach($context->cart->getProducts() as $product) {
        	$itemId = $punchoutCore->getUuid();
        	$product['item_id'] = $itemId;
        	$products[] = $product;
        	$orderedOptions[$itemId] = $product['id_product_attribute'];
        }
        $cartLog = [
            'quote_id'=>$cartData['id_cart'],
            'quote'=> json_encode($cartData),
            'quote_item'=> json_encode($products),
        	'ordered_option' => json_encode($orderedOptions),
        	'uuid'=>$uuid,	
            'log_hash'=>$context->cookie->log_hash,
            'is_live'=> Configuration::get('punchout_mode')
        ];

        foreach($products as $product){
            $id_product_attribute = $product['id_product_attribute']?'#'.$product['id_product_attribute']:'';
            $leadtime = '';
            $unspsc = '';

            $extrinsics = array();
            
            if(isset($product['attributes'])){
                $attributes = $product['attributes'];
                if(strpos($attributes,',')!==false){
                    foreach(explode(',',$attributes) as $attribute){
                        if(strpos($attribute,':')!==false) {
                            $_attributes = explode(':', $attribute);
                            $domain = trim($_attributes[0]);
                            $value = trim($_attributes[1]);
                            $extrinsics[] = array(
                                'domain'=>$domain,
                                'value'=>$value
                            );
                        }
                    }
                }else{
                    $attribute = $attributes;
                    if(strpos($attribute,':')!==false) {
                        $_attributes = explode(':', $attribute);
                        $domain = trim($_attributes[0]);
                        $value = trim($_attributes[1]);
                        $extrinsics[] = array(
                            'domain'=>$domain,
                            'value'=>$value
                        );
                    }
                }
            }
            switch ($punchout_setting->material_source){
                case 'FIXED':
                    $unspsc =  $punchout_setting->material_group_number;
                    break;
                case 'PRODUCT_ATTRIBUTE':
                    foreach(explode(',',$punchout_setting->material_product_attributes) as $attribute){
                        if(isset($product[$attribute])){
                            $unspsc = $product[$attribute];
                            break;
                        }
                    }
                    break;
            }
            if(!$unspsc) {
                $unspsc =  $punchout_setting->material_group_number;
            }
            $product['unspsc'] = $unspsc;

            $classifications = array();
            if($punchout_setting->cxml_order_item_classifications){
                foreach(explode(',',$punchout_setting->cxml_order_item_classifications) as $classification){
                    if(strpos($classification,':')!==false) {
                        $_classifications = explode(':', $classification);
                        $domain = trim($_classifications[0]);
                        $productAttr = trim($_classifications[1]);
                        $defaultAtt = '';
                        if(isset($_classifications[2])) {
                            $defaultAtt = trim($_classifications[2]);
                        }
                        if (isset($product[$productAttr])) {
                            $classifications[] = array(
                                'domain' => $domain,
                                'value' => $product[$productAttr]
                            );
                        } else {
                            $classifications[] = array(
                                'domain' => $domain,
                                'value' => $defaultAtt
                            );
                        }
                    }
                }
            }

            if($punchout_setting->item_leadtime_enable) {
                switch($punchout_setting->item_leadtime_source) {
                    case 'FIXED':
                        $leadtime = $punchout_setting->item_leadtime_fixed;
                        break;
                    case 'PRODUCT_ATTRIBUTE':
                        foreach(explode(',',$punchout_setting->item_leadtime_product_attributes) as $attribute){
                            if(isset($product[$attribute])){
                                $leadtime = $product[$attribute];
                                break;
                            }
                        }
                        break;
                }
            }
            $items[] = array(
            	'item_id'=>$product['item_id'],	
                'qty'=>$product['cart_quantity'],
                'name'=>$product['name'],
                'price'=>$product['price'],
                'product_id'=>$product['reference'],
                'manufacturer_id'=>$product['ean13']?$product['ean13']:'',
                'manufacturer_name'=>'',
                'uom'=>'EA',
                'leadtime'=>$leadtime,
                //'unspsc'=>$unspsc,
                'classifications'=>$classifications,
                'extrinsics'=>$extrinsics

            );
        }
        $data = array(
            'punchout_hash'=>$punchout_hash,
            'buyer_hashcode'=>$customer->buyer_id,
            'supplier_hashcode'=>$supplier_hashcode,
            'is_live'=>$is_live,
            'cart'=>array(
                'cart_id'=>$uuid,
                'currency'=>$context->currency->iso_code,
                'total'=>$context->cart->getOrderTotal(true),
                'items'=>$items
            ),
            'platform'=>'prestashop',
            'new_validation'=>1,
            'ajax_url'=>$context->link->getModuleLink('punchout', 'ajaxcxml'),
            'button_template'=>'<button class="button_large btn btn-default" title="{LABEL}" type="button" id="{BUTTON_ID}"><span>{LABEL}</span></button>',
            'cart_log'=>$cartLog,
            'extra_params'=>[
                'cxml'=>[
                    'version'=>'1.2.028',
                    'xml:lang'=>'en-US'
                ]
            ]
            //'is_debug'=>1
        );
        $entity = $resource->addEntity($data);
        return $entity;

    }

}