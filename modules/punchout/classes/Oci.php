<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutCore.php');

class Oci extends PunchoutCore
{
    private $context;
    private $punchout_setting;
    public function __construct($context) {
        $this->context = $context;
    }
    /**
     * @param $context
     */
    public function generateOciPunchoutForm($position='bottom')
    {
        $context = $this->context;
        $resource = $this->getRestApi('magento-punchout-form');
        $punchoutSetting = new PunchoutSetting();
        $customer = $context->customer;
        $buyer_hashcode = $customer->buyer_id;
        $this->punchout_setting = $punchoutSetting->getPunchoutSetting($buyer_hashcode);
        if(!$this->punchout_setting)
        {
            return;
        }
        $items = array();
        $cartData = $context->cart->getFields();
        $products = $context->cart->getProducts();
        $cartLog = [
            'quote_id'=>$cartData['id_cart'],
            'quote'=> json_encode($cartData),
            'quote_item'=> json_encode($products),
            'log_hash'=>$context->cookie->log_hash,
            'is_live'=> Configuration::get('punchout_mode')
        ];
        
        foreach($products as $product) {
            $items[] = $this->getPunchoutItem($product);
        }
        $data = [
            'setting'=>$this->punchout_setting,
            'hook_url'=>$context->cookie->hook_url,
            'hook_target' => $context->cookie->returntarget,
            'position'=>$position,
            'ajax_url'=>$context->link->getModuleLink('punchout', 'ajaxoci'),
            'platform'=>'prestashop',
            'button_template'=>'<button class="button btn btn-default standard-checkout button-medium" title="{LABEL}" type="button" id="{BUTTON_ID}"><span>{LABEL}</span></button>',
            'items'=>$items,
            'cart_log'=>$cartLog,     
            'is_delete_user'=>$context->cookie->punchout_delete_user,
            
        ];
        $entity = $resource->addEntity($data);
        if($entity)
        {
            return $entity;
        }
        
    }
    
    public function getPunchoutItem($product) 
    {
        $shippingcost_enable = $this->punchout_setting->shippingcost_enable;
        $itemLeadTimeEnable = $this->punchout_setting->item_leadtime_enable;
        $itemVendorEnable = $this->punchout_setting->item_vendor_enable;
        $itemManufactmatEnable = $this->punchout_setting->item_manufactmat_enable;
        $itemServiceEnable = $this->punchout_setting->item_service_enable;
        $material = $this->getPunchoutItemMaterial($product);
        $sku = $this->getProductSku($product);
        $item = [
            'name'=>$product['name'],
            'qty'=>$product['cart_quantity'],
            'price'=>$product['price'],
            'discount_percent'=>0,
            'product_id'=>$product['id_product'],
            'custom_options'=>[],
            'sku'=>$sku,
            'material' => $material,
            'item_vendormat'=>$this->getItemVendormat($product, $sku),
            'tax_class_id'=>$this->getProductTax($product['id_product']),
        ];
        if($itemLeadTimeEnable){
            $item['item_leadtime'] = $this->getProductItemAttribute($product, 'leadtime');
        }
        if($itemVendorEnable) {
            $item['item_vendor'] = $this->getProductItemAttribute($product, 'vendor');
        }
        if($itemManufactmatEnable) {
            $item['item_manufactmat'] = $this->getProductItemAttribute($product, 'manufactmat');
        }
        if($itemServiceEnable) {
            $item['item_service'] = $this->getProductItemAttribute($product, 'service');
        }
        if($shippingcost_enable) {
            $shippingcost = $this->getPunchoutItemShippingCost($product);
            $item['shippingcost'] = $shippingcost;
        }
        return $item;
    }
    
    public function getPunchoutItemShippingCost($product) {
        $shippingcost_source = $this->punchout_setting->shippingcost_source;
        $shippingcost = 0.00;
        switch($shippingcost_source) {
            case self::PRODUCT_ATTRIBUTE:
                foreach(explode(',', $this->punchout_setting->shippingcost_product_attributes) as $attribute) {
                    if(isset($product[trim($attribute)])) {
                        $shippingcost = (float)$product[trim($attribute)];
                        if($shippingcost){
                            break;
                        }
                    }
                }
                break;
            case self::FIXED:
                $shippingcost = $this->punchout_setting->shippingcost_price;
                break;
        }
        return $shippingcost;
    }
    
    
    public function getProductTax($product_id)
    {
        $taxname = 'Not available';
        $product = new Product($product_id);
        $id_tax_rules_group = $product->id_tax_rules_group;
        if($id_tax_rules_group) {
            $tax_rule_group = new TaxRulesGroup($id_tax_rules_group);
            $taxname = $tax_rule_group->name;
        }
        return $taxname;
    }
    
    public function getItemVendormat($product, $default = '')
    {
        $value = '';
        foreach(explode(',', $this->punchout_setting->item_vendormat_product_attributes) as $attribute) {
            if(isset($product[trim($attribute)])) {
                $value = $product[trim($attribute)];
                if($value) {
                    break;
                }
            }
        }
        if(!$value) {
            $value = $default;
        }
        return $value;
    }
    
    public function getPunchoutItemMaterial($product) {
        $material_source = $this->punchout_setting->material_source;
        $default =  $this->punchout_setting->material_group_number;
        $value = '';
        switch ($material_source) {
            case self::MATERIAL_PRODUCT:
                foreach(explode(',', $this->punchout_setting->material_product_attributes) as $attribute) {
                    if(isset($product[trim($attribute)])) {
                        $value = $product[trim($attribute)];
                        if($value) {
                            break;
                        }
                    }
                }
            break;
            case self::FIXED:
                $value = $this->punchout_setting->material_group_number;
                break;
        }
        if(!$value) {
            $value = $default;
        }
        return $value;
    }
    
    
    
    public function getProductSku($product) {
        $sku = '';
        if($this->punchout_setting->item_sku) {
            $sku = $product[$this->punchout_setting->item_sku];
        }
        return $sku;
    }
    
    public function getProductItemAttribute($product, $type, $default = '')
    {
        $value = '';
        if($this->punchout_setting->{"item_{$type}_enable"}) {
            switch($this->punchout_setting->{"item_{$type}_source"}) {
                case self::PRODUCT_ATTRIBUTE:
                    foreach(explode(',', $this->punchout_setting->{"item_{$type}_product_attributes"}) as $attribute){
                        if(isset($product[trim($attribute)])) {
                            $value = $product[trim($attribute)];
                            if($value) {
                                break;
                            }
                        }
                    }
                case self::FIXED:
                    $value = $this->punchout_setting->{"item_{$type}_fixed"};
                    break;
            }
        }else {
            $value = $this->punchout_setting->{"item_{$type}_fixed"};
        }
        if(!$value) {
            $value = $default;
        }
        return $value;
    }
}