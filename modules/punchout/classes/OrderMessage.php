<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PunchoutSetting.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Cxml.php');
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Oci.php');

class OrderMessage extends Entity
{
    private $context;
    public function __construct($context) {
        $this->context = $context;        
    }
    
    public function isPunchoutUser()
    {
        $punchout_hash = $this->context->cookie->punchout_hash;
        $hook_url = $this->context->cookie->hook_url;
        if(($punchout_hash && Configuration::get('punchout_cxml_enable')) ||
            ($hook_url && Configuration::get('punchout_oci_enable'))) {
            return true;
        }
        return false;
        
    }
    
    public function generatePunchoutForm()
    {
        $punchout_hash = $this->context->cookie->punchout_hash;
        $hook_url = $this->context->cookie->hook_url;
        if($punchout_hash && Configuration::get('punchout_cxml_enable')) {
            $cxml = new Cxml($this->context);
            return $cxml->generateCxmlPunchoutForm();
        }
        if($hook_url && Configuration::get('punchout_oci_enable')) {
            $oci = new Oci($this->context);
            return $oci->generateOciPunchoutForm();
        }
            
    }
    
}