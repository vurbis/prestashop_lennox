<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
class PunchoutSetting extends Entity
{
    public function getPunchoutSetting($buyer_hashcode)
    {
        if($buyer_hashcode) {
            $resource = $this->getRestApi('punchout-setting');
            $supplier_hashcode = Configuration::get('punchout_supplier_code');
            $params = array('supplier_hashcode' => $supplier_hashcode, 'buyer_hashcode' => $buyer_hashcode);
            $entities = $resource->getEntities($params);
            if ($entities) {
                return $entities->_embedded->punchout_setting[0];
            }
        }
        return false;

    }
}