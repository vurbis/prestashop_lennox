<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
class Buyer extends Entity
{
    protected $_options;
    public function getBuyers()
    {
        $mod = Module::getInstanceByName('punchout');
        $resource = $this->getRestApi('buyer');
        $supplier_hashcode = Configuration::get('punchout_supplier_code');
        $params = array('supplier_hashcode' => $supplier_hashcode);
        $entities = $resource->getEntities($params);
        $page_count = $entities->page_count;
        if ($this->_options === null) {
            $this->_options = array();
        }
        $this->_options[] = array(
            'value' => '',
            'label' => $mod->l('Choose a Buyer')
        );
        for ($page = 1; $page <= $page_count; $page++) {
            $params = array('supplier_hashcode' => $supplier_hashcode, 'page' => $page);
            $entities = $resource->getEntities($params);
            foreach ($entities->_embedded->buyer as $buyer) {
                $this->_options[] = array(
                    'value' => $buyer->buyer_hashcode,
                    'label' => $buyer->name
                );
            }
        }
        return $this->_options;
    }
}