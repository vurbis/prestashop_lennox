<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Entity.php');
class PunchoutCore extends Entity 
{
    const RESOURCE_BUYER                        = 'buyer';
    const RESOURCE_CXML_PUNCHOUT_SETUP_REQUEST  = 'cxml-punchout-setup-request';
    const RESOURCE_PUNCHOUT_SETTING             = 'punchout-setting';
    const RESOURCE_PUNCHOUT_LOG                 = 'punchout-logs';
    const CXML_CARTLOG							= 'cxmlcartlog';
    const RESOURCE_CXML_PUNCHOUT_ORDER_MESSAGE  = 'cxml-punchout-order-message';
    const RESOURCE_CXML_PUNCHOUT_ORDER_SETUP_REQUEST = 'cxml-punchout-order-setup-request';
    const RESOURCE_CXML_PUNCHOUT_EINVOICE       = 'cxml-punchout-invoice';
    const TYPE_LOGIN                            = 'login';
    const TYPE_EINVOICE                         = 'einvoice';
    const TYPE_ORDER                            = 'order';
    const PUNCHOUT_TYPE_CXML                    = 'cxml';
    const PUNCHOUT_TYPE_OCI                     = 'oci';

    const MATERIAL_CATEGORY = 'CATEGORY';
    const MATERIAL_PRODUCT = 'PRODUCT_ATTRIBUTE';

    const FIXED                                 = 'FIXED';
    const PRODUCT_ATTRIBUTE                     = 'PRODUCT_ATTRIBUTE';
    const SOURCE_URL                            = 'FROM_URL';

    const PLATFORM                              = 'magento';

    const CHARS_LOWERS                          = 'abcdefghijklmnopqrstuvwxyz';
    const CHARS_UPPERS                          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const CHARS_DIGITS                          = '0123456789';


    public function getRandomString($len, $chars = null)
    {
        if (is_null($chars)) {
            $chars = self::CHARS_LOWERS . self::CHARS_UPPERS . self::CHARS_DIGITS;
        }
        mt_srand(10000000*(double)microtime());
        for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $lc)];
        }
        return $str;
    }
    
    public function getProductCategory($product_id) 
    {
        $data = [];
        $sql = "SELECT id_category FROM "._DB_PREFIX_."category_product WHERE id_product={$product_id}";
        if ($results = Db::getInstance()->ExecuteS($sql)){
            foreach($results as $result) {
                $data[] = $result['id_category'];
            }
        }
        return $data;
    }
    
    public function createLoginLog($data)
    {
        $resource = $this->getRestApi('punchout-logs');
        $logData = [
            'supplier_hashcode' => $data['supplier_hashcode'],
            'buyer_hashcode' => $data['buyer_hashcode'],
            'customer_id'=>$data['customer_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'request_content' => json_encode($_SERVER),
            'cart_content' => '',
            'is_live'=>Configuration::get('punchout_mode')
        ];
        $entity = $resource->addEntity($logData);
        return $entity;
    }
    
    public function removeCxmlCartLog($uuid)
    {
    	$resource = $this->getRestApi(self::CXML_CARTLOG);
    	try {
    		$resource->deleteEntity($uuid);
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }
    
    public function getCxmlCartLog($uuid)
    {
    	$resource = $this->getRestApi(self::CXML_CARTLOG);
    	try {
    		$entity = $resource->getEntity($uuid);
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    	return $entity;
    }
    
    public function getUuid()
    {
    	mt_srand((double)microtime()*10000);
    	$charid = md5(uniqid(rand(), true));
    	$hyphen = chr(45);
    	$uuid = ''
    			.substr($charid, 0, 8).$hyphen
    			.substr($charid, 8, 4).$hyphen
    			.substr($charid,12, 4).$hyphen
    			.substr($charid,16, 4).$hyphen
    			.substr($charid,20,12);
    			return strtoupper($uuid);
    }

}
