<?php
include_once (_PS_MODULE_DIR_.'punchout'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Buyer.php');
class ModuleSetting extends Buyer
{
    public function generateTaxTemplate()
    {
        $content[] = array("Tax Name", "Punchout Display");
        $result = Db::getInstance()->executeS('
			SELECT distinct tl.`name`
			FROM '._DB_PREFIX_.'tax_lang tl');
        foreach($result as $tax) {
            $arr = [];
            $arr[] = $tax['name'];
            $arr[] = '';
            $content[] = $arr;
        }
        header('Content-Type: application/csv');
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="punchout_taxes_'.date('YmdHis').'.csv";');

        $f = fopen('php://output', 'rb');
        fwrite($f,pack("CCC",0xef,0xbb,0xbf));

        foreach ($content as $line) {
            fputcsv($f, $line, ',');
        }
        fclose($f);
        exit();
    }
    
    public function importBuyerTaxes($buyer, $taxUploadedFile, $overwrite) 
    {
        $supplier_hashcode = Configuration::get('punchout_supplier_code');
        $resource = $this->getRestApi('punchout-tax');
        $count = 0;
        if($buyer) {
            $index = 0;
            if (($handle = fopen($taxUploadedFile, "r")) !== FALSE) {
                while (($row = fgetcsv($handle, 4096, ",")) !== FALSE) {
                    if($index > 0) {
                        $data = [
                            'supplier_hashcode'=>$supplier_hashcode,
                            'buyer_hashcode'=>$buyer,
                            'display_name'=>$row[0],
                            'punchout_code'=>$row[1],
                            'overwrite'=>$overwrite
                        ];
                        $entity = $resource->addEntity($data);
                        if($entity) {
                            $count ++;
                        }
                    }
                    $index++;
                }
            }
        }
        return $count;
    }
}