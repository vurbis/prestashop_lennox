<?php
namespace Vurbis\API\Rest;
use Vurbis\API\Exception\VurbisMarketplaceException;

class Client
{
    protected $oauth_client;
    protected $resource_url;
    public function __construct($resource_url)
    {
        $this->resource_url = $resource_url;
    }

    public function setOauthClient($oauth_client)
    {
        $this->oauth_client = $oauth_client;
    }
    public function getEntities($params = array())
    {
        $q = array();
        foreach($params as $key=>$val)
        {
            $q[] = $key.'='.$val;
        }
        //echo $this->resource_url."?".implode('&',$q);
        $response = $this->requestCurl($this->resource_url."?".implode('&',$q));
        return $response;
    }

    public function getEntity($id)
    {
        $response = $this->requestCurl($this->resource_url."/".$id);

        return $response;
    }

    public function updateEntity($id,$data)
    {
        $response = $this->requestCurl($this->resource_url.'/'.$id,'PUT',$data);
        return $response;
    }

    public function addEntity($data)
    {

        $response = $this->requestCurl($this->resource_url,'POST',$data);
        return $response;
    }

    public function deleteEntity($id)
    {
        $response = $this->requestCurl($this->resource_url.'/'.$id,'DELETE');
        return $response;
    }

    private function _getCurlHeader()
    {
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: '.$this->oauth_client->getTokenType().' '.$this->oauth_client->getAccessToken(),
        );
       return $headers;
    }

    private function requestCurl($url,$method = 'GET',$data = null)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $this->_getCurlHeader());
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

        switch($method) {
            case 'GET':
                break;
            case 'POST':
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'PUT':
                curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'DELETE':
                curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }

        $response = json_decode(curl_exec($handle));
        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        switch($code)
        {
            case 201:   // created
            case 200:   // get collection, get
                        // get entity
                        // update
                return $response;
                break;
            case 204: // deleted
                return true;
                break;
            case 422:
                $message = "\n";
                $messages = get_object_vars($response->validation_messages);
                foreach ($messages as $field => $text) {
                    foreach($text as $type=>$string) {
                        $message .= '[' . $field . ']: [' . $type . ']: ' . $string . "\n";
                    }
                }
                throw new \Exception($message, $code);
                break;
            default:
                throw new \Exception($response->title.': '.$response->detail, $code);
                break;

        }

    }

}