<?php
namespace Vurbis\API\Oauth2;
use Vurbis\API\Exception\VurbisMarketplaceException;
class Client
{
    protected $oauth_uri;
    protected $client_id;
    protected $client_secret;
    protected $username;
    protected $password;

    protected $access_token ;
    protected $token_type;
    protected $refresh_token;
    public function __construct($oauth_uri,$client_id,$client_secret,$username,$password)
    {
        $this->oauth_uri = $oauth_uri;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->username = $username;
        $this->password = $password;

    }

    public function authorize()
    {
        if($this->access_token)
            return;
        $data  = array(
            'grant_type' => 'password',
            'username' => $this->username,
            'password' => $this->password,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
        );
        $response = $this->curl_post($this->oauth_uri,$data);

        if(!isset($response['access_token']))
        {
            throw new VurbisMarketplaceException($response['detail'],$response['status']);
            return;
        }
        $this->access_token = $response['access_token'];
        $this->token_type = $response['token_type'];
        $this->refresh_token = $response['refresh_token'];
    }

    public function refresh_token()
    {
        $data = array(
            "grant_type"=> "refresh_token",
            "refresh_token"=> $this->refresh_token,
            "client_id"=>$this->client_id,
            "client_secret"=> $this->client_secret
        );
        $response = $this->curl_post($this->oauth_uri,$data);

        if(!isset($response['access_token']))
        {
            throw new VurbisMarketplaceException($response['detail'],$response['status']);
            return;
        }
    }

    public function getAccessToken()
    {

        return $this->access_token;
    }

    public function getTokenType()
    {

        return $this->token_type;
    }

    public function getRefreshToken()
    {

        return $this->refresh_token;
    }

    public function curl_post($url,$data)
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($data));

        $resp = json_decode(curl_exec($req), true);
        curl_close($req);
        return $resp;
    }
}